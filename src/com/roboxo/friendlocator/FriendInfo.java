package com.roboxo.friendlocator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.roboxo.friendlocator.Database.FriendLocatorDB;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class FriendInfo extends Activity implements OnClickListener {

	ArrayList<User> users;
	Button LK, IL, bSharing;
	String phone;
	String allowed;
	FriendLocatorDB db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friendinfo);
		Intent i = getIntent();
		TextView name = (TextView) findViewById(R.id.tvInfoName);
		phone = i.getStringExtra("phone");
		allowed = i.getStringExtra("allowed");
		bSharing = (Button) findViewById(R.id.bShareLoc);
		name.setText(i.getStringExtra("name"));
		LK = (Button) findViewById(R.id.getLastKnown);
		IL = (Button) findViewById(R.id.getInstantLocation);
		LK.setOnClickListener(this);
		IL.setOnClickListener(this);
		bSharing.setOnClickListener(this);
		db = new FriendLocatorDB(getApplicationContext());
		db.open();

		if (allowed.equals("1")) {
			bSharing.setText("Allow Sharing Location");
		} else {
			bSharing.setText("Disallow Sharing Location");
		}
		db.close();

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.getLastKnown) {
			if (allowed.equals("1"))
				new getLastKnownLocation().execute();
			else
				Toast.makeText(getApplicationContext(), "Not Allowed to view Location", Toast.LENGTH_LONG).show();
		} else if (v.getId() == R.id.getInstantLocation) {
			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(phone, null,
					"FriendLocator##GetLocationRequestplz", null, null);
		} else if (v.getId() == R.id.bShareLoc) {
			db.open();
			if (allowed.equals("1")) {
				allowed = "-1";
				db.disallowFriend(phone);
				bSharing.setText("Allow Sharing Location");
			} else {
				allowed = "1";
				db.allowFriend(phone);
				bSharing.setText("Disallow Sharing Location");
			}
			new updateAllowStatusOnline().execute();
		}
	}

	private class updateAllowStatusOnline extends AsyncTask<Void, String, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			SharedPreferences p = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			final String myphone = p.getString("phone", "");
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://friendlocator.web44.net/updateAllowStatusOnline.php");

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
			nameValuePairs.add(new BasicNameValuePair("myPhone", myphone
					.toString()));
			nameValuePairs.add(new BasicNameValuePair("friend", phone
					.toString()));
			nameValuePairs.add(new BasicNameValuePair("status", allowed
					.toString()));

			try {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				String abc = (reader.readLine());
				publishProgress(abc);
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			Toast.makeText(getApplicationContext(), values[0],
					Toast.LENGTH_SHORT).show();
			}

	}

	private class getLastKnownLocation extends AsyncTask<Void, String, Void> {

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://friendlocator.web44.net/getFriendLocation.php");
			// publishProgress(arg0[0] +arg0[1] +simID);
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("friendPhone", phone
					.toString()));

			try {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				String abc = (reader.readLine());
				publishProgress(abc);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			Toast.makeText(getApplicationContext(), values[0],
					Toast.LENGTH_SHORT).show();
			String abc[] = new String[3];
			abc = values[0].split("#");
			Intent i = new Intent(FriendInfo.this, ShowMap.class);
			i.putExtra("lat", abc[0]);
			i.putExtra("lng", abc[1]);
			i.putExtra("name", getIntent().getStringExtra("name"));
			startActivity(i);
		}
	}
}
