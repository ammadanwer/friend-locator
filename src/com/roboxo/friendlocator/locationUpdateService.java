package com.roboxo.friendlocator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class locationUpdateService extends Service implements LocationListener {

	LocationManager locationManager;
	String availableProvider;
	String simID;
	Location locate;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (locationManager.isProviderEnabled(locationManager.GPS_PROVIDER))
			availableProvider = locationManager.GPS_PROVIDER;

		else
			availableProvider = locationManager.NETWORK_PROVIDER;
		SharedPreferences p = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		simID = p.getString("simID", "");
		locationManager.getProvider(locationManager.NETWORK_PROVIDER);

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub

		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 5 * 60 * 1000, 10, this);
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onLocationChanged(Location loc) {
		// TODO Auto-generated method stub
		
		new LocationUpdate().execute(loc);
		
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		if (locationManager.isProviderEnabled(locationManager.GPS_PROVIDER))
			availableProvider = locationManager.GPS_PROVIDER;

		else
			availableProvider = locationManager.NETWORK_PROVIDER;
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		if (locationManager.isProviderEnabled(locationManager.GPS_PROVIDER))
			availableProvider = locationManager.GPS_PROVIDER;

		else
			availableProvider = locationManager.NETWORK_PROVIDER;
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}
	private class LocationUpdate extends AsyncTask<Location, Void, Void>
	{

		@Override
		protected Void doInBackground(Location... loc) {
			// TODO Auto-generated method stub
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://friendlocator.web44.net/updateLocation.php");

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
			nameValuePairs.add(new BasicNameValuePair("simID", simID.toString()));
			nameValuePairs
					.add(new BasicNameValuePair("lat", loc[0].getLatitude()+""));
			nameValuePairs.add(new BasicNameValuePair("longs", loc[0].getLongitude()+""));

			try {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				BufferedReader reader = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				String abc = (reader.readLine());
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
	}

}
