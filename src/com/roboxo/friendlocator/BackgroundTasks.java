package com.roboxo.friendlocator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class BackgroundTasks {
	public int status;
	TelephonyManager mTelephonyMgr;
	String Name, Number;
	Context context;

	public BackgroundTasks() {

	}

	public void registerPhoneAndName(String name, String number, Context c) {
		Name = name;
		Number = number;
		context = c;

		mTelephonyMgr = (TelephonyManager) c
				.getSystemService(Context.TELEPHONY_SERVICE);
		RegisterPhoneAndName rpn = new RegisterPhoneAndName();
		try {
			int st = rpn.execute().get();
			if (st == HttpStatus.SC_OK) {
				Toast.makeText(c, "You are registered", Toast.LENGTH_LONG)
						.show();
			} else
				Toast.makeText(c, "Problem in registeration", Toast.LENGTH_LONG)
						.show();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public class RegisterPhoneAndName extends AsyncTask<Void, Void, Integer> {

		@Override
		protected Integer doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			SharedPreferences p = PreferenceManager
					.getDefaultSharedPreferences(context
							.getApplicationContext());

			p.edit().putString("name", Name).commit();
			p.edit().putString("phone", Number).commit();

			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://friendlocator.web44.net/register.php");
			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						3);
				nameValuePairs.add(new BasicNameValuePair("name", Name));
				nameValuePairs.add(new BasicNameValuePair("phonenum", Number));

				nameValuePairs.add(new BasicNameValuePair("simID",
						mTelephonyMgr.getSimSerialNumber().toString()));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				status = response.getStatusLine().getStatusCode();
				//
				// Toast.makeText(GetInfo.this, status, Toast.LENGTH_LONG)
				// .show();
			} catch (ClientProtocolException e) {
			} catch (IOException e) {
			}
			return status;
		}
	}
}
