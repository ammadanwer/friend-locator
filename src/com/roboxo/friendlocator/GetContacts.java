package com.roboxo.friendlocator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.ads.AdView;
import com.roboxo.friendlocator.Database.FriendLocatorDB;

import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.InputStream;
import java.io.Reader;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONTokener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

public class GetContacts extends AsyncTask<Void, Void, Void>{
	public Context con;
	JSONObject user = new JSONObject();
	EditText etphone, etname;
	Button btsubmit;
	TelephonyManager mTelephonyMgr;
	int status;
	NetworkInfo ni;
	ConnectivityManager cm;
	String abc;
	AdView adView;
	FriendLocatorDB db;
	JSONArray finalResult;
	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		db = new FriendLocatorDB(con);
		try {
			sendContacts();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	private void sendContacts() throws JSONException {
		// TODO Auto-generated method stub
		Cursor phones = con.getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
				null, null);
		ArrayList<String> mycontactsname = new ArrayList<String>();
		ArrayList<String> mycontactsphone = new ArrayList<String>();
		JSONArray d = new JSONArray();
		user = new JSONObject();
		JSONObject jdata = new JSONObject();
		JSONObject job = new JSONObject();

		while (phones.moveToNext()) {
			String name = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
			String phoneNumber = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			mycontactsname.add(name);
			mycontactsphone.add(phoneNumber);
			// Toast.makeText(getApplicationContext(), name, 300000).show();
			jdata = new JSONObject();
			jdata.put("name", name);
			jdata.put("phoneNumber", phoneNumber);
			d.put(jdata);

		}
		user.put("data", d);

		phones.close();
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://friendlocator.web44.net/getContacts.php");
		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
					1);
			nameValuePairs.add(new BasicNameValuePair("myjson", user
					.toString()));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			status = response.getStatusLine().getStatusCode();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));
			abc = (reader.readLine());
			
			JSONObject jb = new JSONObject(abc);
			finalResult = jb.getJSONArray("data");
		} catch (ClientProtocolException e) {

			 
		} catch (IOException e) {

			

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.open();
		for (int i = 0; i < finalResult.length(); i++) {

			String joname = null;
			String jophone = null;
			try {
				
				joname = ((JSONObject) finalResult.get(i))
						.getString("name");
				jophone = ((JSONObject) finalResult.get(i))
						.getString("phoneNumber");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (joname != null && jophone != null && !db.friendExists(jophone)){
				db.insertFriendTable(joname, jophone, -1);
			}

		}
		db.close();
		return;
		
	}
	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		
		
		super.onPostExecute(result);
		
	}

}
