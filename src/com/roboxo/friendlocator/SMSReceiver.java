package com.roboxo.friendlocator;

import com.roboxo.friendlocator.Database.FriendLocatorDB;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.sax.StartElementListener;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class SMSReceiver extends BroadcastReceiver {
	Context c;
	@Override
	public void onReceive(Context context, Intent intent) {
		c = context;
		Bundle extras = intent.getExtras();

		Object[] pdus = (Object[]) extras.get("pdus");
		for (Object pdu : pdus) {
			SmsMessage msg = SmsMessage.createFromPdu((byte[]) pdu);
			String origin = msg.getOriginatingAddress();
			String body = msg.getMessageBody();

			// Parse the SMS body
			if (body.equals("FriendLocator##GetLocationRequestplz")) {

				Intent i = new Intent(context, SendMessageService.class);
				i.putExtra("destination", origin);
				context.startService(i);
				// Stop it being passed to the main Messaging inbox

				abortBroadcast();
			} else if (body.contains("FriendLocator##LocationResponseplz###")) {
				// Stop it being passed to the main Messaging inbox
				Toast.makeText(context, body,
						Toast.LENGTH_LONG).show();
				String text[] = new String[3];
				text = body.split("###");
				Toast.makeText(context, text[1], Toast.LENGTH_SHORT).show();

				if(text[1].equals("Sorry not allowed"))
				{
					Toast.makeText(context, "You are not allowed to view this contact's location", Toast.LENGTH_SHORT).show();
				}
				else{
					
					new showLocation().execute(text[1],origin);
				}
				abortBroadcast();
			}
		}
	}
	private class showLocation extends AsyncTask<String, String, Void>
	{

		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub
			String loc[] = new String[2];
			loc = params[0].split("#");
			//now get the name of the friend
			FriendLocatorDB db = new FriendLocatorDB(c);
			db.open();
			String name = db.getCorrespondingFriendName(params[1]);  // change to this before submission
			//String name = db.getCorrespondingFriendName("+923044978043");
			db.close();
			publishProgress(loc[0],loc[1],name);
			return null;
		}
		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			
			Intent i = new Intent(c,ShowMap.class);
			i.putExtra("lat", values[0]);
			i.putExtra("lng", values[1]);
			i.putExtra("name", values[2]);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			c.startActivity(i);
		}
	}
}
