package com.roboxo.friendlocator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.roboxo.friendlocator.Database.FriendLocatorDB;

import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class checkForRequest extends Service {

	FriendLocatorDB db;
	static MainActivity ma;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		db = new FriendLocatorDB(getApplicationContext());
		Toast.makeText(getApplicationContext(), "check for request on create",
				Toast.LENGTH_SHORT).show();

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		new checkIncomingRequest().execute();
		
		Toast.makeText(getApplicationContext(), "check for request on start",
				Toast.LENGTH_SHORT).show();
		return super.onStartCommand(intent, flags, startId);
	}

	private class checkIncomingRequest extends AsyncTask<Void, String, Void> {

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub

			SharedPreferences p = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			final String simID = p.getString("phone", "");
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://friendlocator.web44.net/checkIncomingRequest.php");

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("myPhone", simID
					.toString()));

			try {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				String abc = (reader.readLine());
				if (!abc.equals("no request")) {
					try {
						JSONObject jo = new JSONObject(abc);
						JSONArray ja = jo.getJSONArray("data");
						for (int i = 0; i < ja.length(); i++){
							db.open();
							String num = ((JSONObject) ja.get(i)).getString(
									"phone").toString();
							String name = db.getCorrespondingFriendName(num);
							publishProgress(name,num);
						}

						// publishProgress(jo.toString());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			Toast.makeText(getApplicationContext(), values[0],
					Toast.LENGTH_SHORT).show();

			showConfirmatoryAlert(values[0],
					values[1]);

		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			new checkOutgoingRequestStatus().execute();
			//stopSelf();
		}

	}

	private class UpdateResponseOfRequest extends
			AsyncTask<String, String, Void> {

		@Override
		protected Void doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			SharedPreferences p = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			final String simID = p.getString("phone", "");
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://friendlocator.web44.net/updateResponseOfRequest.php");
			// publishProgress(arg0[0] +arg0[1] +simID);
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
			nameValuePairs.add(new BasicNameValuePair("myPhone", simID
					.toString()));
			nameValuePairs.add(new BasicNameValuePair("friendPhone", arg0[0]
					.toString()));
			nameValuePairs.add(new BasicNameValuePair("allowStatus", arg0[1]
					.toString()));

			try {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				String abc = (reader.readLine());
				publishProgress(abc);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			Toast.makeText(getApplicationContext(), values[0],
					Toast.LENGTH_SHORT).show();

		}

	}

	private void showConfirmatoryAlert(String name, final String number) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ma);

		// set title
		alertDialogBuilder.setTitle("New Request");

		// set dialog message
		alertDialogBuilder
				.setMessage(
						name
								+ " has requesed you to allow sharing locations!\n\t Do You want to Allow?")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								db.open();
								db.allowFriend(number);
								new UpdateResponseOfRequest().execute(number,
										"1");
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						db.open();
						db.disallowFriend(number);
						new UpdateResponseOfRequest().execute(number, "-1");
					}
				});
		//
		alertDialogBuilder.show();
	}

	private class checkOutgoingRequestStatus extends
			AsyncTask<Void, String, Void> {

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			db = new FriendLocatorDB(getApplicationContext());
			db.open();
			
			Cursor c = db.getFriendswithSentRequest();
			if(c.getCount()==0)
				return null;
			JSONArray ja = new JSONArray();
			for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
				ja.put(c.getString(1));
			JSONObject jo = new JSONObject();
			try {
				jo.put("users", ja);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			//send to server
			SharedPreferences p = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			final String myphone = p.getString("phone", "");
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://friendlocator.web44.net/checkOutgoingRequestStatus.php");

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("myPhone", myphone
					.toString()));
			nameValuePairs.add(new BasicNameValuePair("friends", jo
					.toString()));
			publishProgress(jo.toString());
			JSONObject result;
			JSONArray jsnarray;
			
				try {
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost);
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(response.getEntity().getContent()));
					String abc = (reader.readLine());
					publishProgress(abc);
					result = new JSONObject(abc);
					jsnarray = new JSONArray();
					jsnarray = result.getJSONArray("data");
					publishProgress(jsnarray.toString());
					for(int i =0;i<jsnarray.length();i++)
					{
						if(((JSONObject)jsnarray.get(i)).getString("status").equals("1"))
							db.allowFriend(((JSONObject)jsnarray.get(i)).getString("phone"));
						else
							db.disallowFriend(((JSONObject)jsnarray.get(i)).getString("phone"));
					}
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
			return null;
		}
		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			Toast.makeText(getApplicationContext(), values[0], Toast.LENGTH_SHORT).show();
		}
	}
}
