package com.roboxo.friendlocator;

import java.util.ArrayList;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.roboxo.friendlocator.Database.FriendLocatorDB;

public class MainActivity extends Activity  {

	ArrayList<User> users = new ArrayList<User>();
	FriendLocatorDB db;
	AdView adView;
	ListView lv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initialize();
		lv = (ListView) findViewById(R.id.userList);
		setUpListView();
		
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent i = new Intent(MainActivity.this,FriendInfo.class);
				i.putExtra("name", users.get(arg2).name);
				i.putExtra("phone", users.get(arg2).phone);
				i.putExtra("allowed", users.get(arg2).allowed+"");
				startActivity(i);
			}
		});
	}

	private void setUpListView() {
		// TODO Auto-generated method stub
		new setListView().execute();
	}

	private void initialize() {
		// TODO Auto-generated method stub
		adView = (AdView) findViewById(R.id.adViewMain);
		TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		 AdRequest adRequest = new AdRequest();
				 adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
				 adRequest.addTestDevice(mTelephonyMgr.getDeviceId());

				 //Initiate a generic request to load it with an ad
		adView.loadAd(adRequest);
//		SocialAuthAdapter adapter = new SocialAuthAdapter(new ResponseListener());
//	    adapter.addProvider(Provider.FACEBOOK, R.drawable.facebook);
//	    adapter.addProvider(Provider.TWITTER, R.drawable.twitter);
		
		
		Intent i = new Intent(MainActivity.this,locationUpdateService.class);
		startService(i);
		startIncomingRequestService();
	}



	private void startIncomingRequestService() {
		// TODO Auto-generated method stub
		checkForRequest.ma = this;
//		AlarmManager am = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(getApplicationContext(),checkForRequest.class);
		startService(i);
		GetContacts gc = new GetContacts();
		gc.con = this;
		gc.execute();
//		PendingIntent pi = PendingIntent.getService(getApplicationContext(), 0, i, 0);
//		am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 5*60*1000, pi);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

 private class setListView extends AsyncTask<Void, Void, Void>
 {

	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		db = new FriendLocatorDB(MainActivity.this);
		db.open();
		Cursor c = db.getFriendTable();
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
		{
			users.add(new User(c.getString(0),c.getString(1),c.getInt(2)));
			
		}
		return null;
	}
	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		lv.setAdapter(new FriendListAdapter(MainActivity.this, R.layout.list_item, users));
	}
	 
 }

}
