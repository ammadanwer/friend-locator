package com.roboxo.friendlocator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

public class SplashScreen extends Activity {

	Intent i;
	SharedPreferences p;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		p = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		final String simID = p.getString("simID", "");
		final String phone = p.getString("phone", "");
		final String name = p.getString("name", "");
		Thread timer = new Thread() {
			public void run() {
				try {
					sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();

				} finally {
					if (simID.equals("") || phone.equals("") || name.equals("")) {
						TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
						String simno = mTelephonyMgr.getSimSerialNumber();
						p.edit().putString("simID", simno).commit();
						 i = new Intent(SplashScreen.this, GetInfo.class);
					} else {
						 i = new Intent(SplashScreen.this, MainActivity.class);
						
					}
					startActivity(i);
					SplashScreen.this.finish();
				}
			}
		};
		timer.start();
	}
}
