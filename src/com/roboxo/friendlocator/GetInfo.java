package com.roboxo.friendlocator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.ads.*;
import com.roboxo.friendlocator.Database.FriendLocatorDB;

public class GetInfo extends Activity implements OnClickListener {
	EditText etphone, etname;
	Button btsubmit;
	TelephonyManager mTelephonyMgr;
	int status;
	NetworkInfo ni;
	ConnectivityManager cm;
	String abc;
	AdView adView;
	FriendLocatorDB db;
	JSONObject user;
	JSONArray finalResult;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.getinfo);
		initialize();
	}

	private void initialize() {
		// TODO Auto-generated method stub
		etphone = (EditText) findViewById(R.id.etphone);
		etname = (EditText) findViewById(R.id.etname);
		btsubmit = (Button) findViewById(R.id.btSubmitinfo);

		mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		btsubmit.setOnClickListener(this);
		cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		adView = (AdView) findViewById(R.id.adView);
		db = new FriendLocatorDB(this);
		 AdRequest adRequest = new AdRequest();
		 adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
		 adRequest.addTestDevice(mTelephonyMgr.getDeviceId());

		// Initiate a generic request to load it with an ad
		 adView.loadAd(adRequest);

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0.getId() == R.id.btSubmitinfo) {
			ni = cm.getActiveNetworkInfo();

			if (ni == null) {
				Toast.makeText(GetInfo.this, "No internet Connectivity",
						Toast.LENGTH_SHORT).show();
				return;
			}
			if (!nameEmpty() && !phoneEmpty()) {
				BackgroundTasks bts = new BackgroundTasks();
				bts.registerPhoneAndName(etname.getText().toString(), etphone
						.getText().toString(), GetInfo.this);
				if (bts.status == HttpStatus.SC_OK) {
					Toast.makeText(GetInfo.this, "Registered Successfully",
							Toast.LENGTH_SHORT).show();
					GetFriendsAndPutInDB gfpd = new GetFriendsAndPutInDB();
					gfpd.execute();
					//sendContacts();
				}
			} else
				Toast.makeText(GetInfo.this, "Name and number cannot be empty",
						Toast.LENGTH_LONG).show();
		}
	}

	private void sendContacts() throws JSONException {
		// TODO Auto-generated method stub
		Cursor phones = getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
				null, null);
		ArrayList<String> mycontactsname = new ArrayList<String>();
		ArrayList<String> mycontactsphone = new ArrayList<String>();
		JSONArray d = new JSONArray();
		user = new JSONObject();
		JSONObject jdata = new JSONObject();
		JSONObject job = new JSONObject();

		while (phones.moveToNext()) {
			String name = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
			String phoneNumber = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			mycontactsname.add(name);
			mycontactsphone.add(phoneNumber);
			// Toast.makeText(getApplicationContext(), name, 300000).show();
			jdata = new JSONObject();
			jdata.put("name", name);
			jdata.put("phoneNumber", phoneNumber);
			d.put(jdata);

		}
		user.put("data", d);

		phones.close();
//		Toast.makeText(getApplicationContext(), user.toString(),
//				Toast.LENGTH_LONG).show();
		// etname.setText(user.toString());
		// sendJSON(user);
		
		
		GetFriendsAndPutInDB gfpd = new GetFriendsAndPutInDB();
		gfpd.execute();
		
	}


	private boolean phoneEmpty() {
		// TODO Auto-generated method stub
		return etphone.getText().equals("");
	}

	private boolean nameEmpty() {
		// TODO Auto-generated method stub
		return etname.getText().equals("");
	}

	public class GetFriendsAndPutInDB extends AsyncTask<Void, String, Integer> {

		@Override
		protected Integer doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			//send contacts start
			Cursor phones = getContentResolver().query(
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
					null, null);
			ArrayList<String> mycontactsname = new ArrayList<String>();
			ArrayList<String> mycontactsphone = new ArrayList<String>();
			JSONArray d = new JSONArray();
			user = new JSONObject();
			JSONObject jdata = new JSONObject();
			JSONObject job = new JSONObject();

			while (phones.moveToNext()) {
				
				String name = phones
						.getString(phones
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
				String phoneNumber = phones
						.getString(phones
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				//publishProgress(name + " : " + phoneNumber);
				mycontactsname.add(name);
				mycontactsphone.add(phoneNumber);
				// Toast.makeText(getApplicationContext(), name, 300000).show();
				jdata = new JSONObject();
				try {
					jdata.put("name", name);
					jdata.put("phoneNumber", phoneNumber);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				d.put(jdata);

			}
			try {
				user.put("data", d);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			phones.close();
			//send contacts end
			
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://friendlocator.web44.net/getContacts.php");
			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						1);
				nameValuePairs.add(new BasicNameValuePair("myjson", user
						.toString()));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				status = response.getStatusLine().getStatusCode();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				
				abc = (reader.readLine());
				publishProgress(abc);
				JSONObject jb = new JSONObject(abc);
				finalResult = jb.getJSONArray("data");
			} catch (ClientProtocolException e) {

				 Toast.makeText(GetInfo.this,
				 "Client Protocol Exception" + e.toString(),
				 Toast.LENGTH_LONG).show();
			} catch (IOException e) {

				 Toast.makeText(GetInfo.this, "IO Exception " + e.toString(),
				 Toast.LENGTH_LONG).show();

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			db = new FriendLocatorDB(getApplicationContext());
			db.open();
			publishProgress(finalResult.length() +"");
			for (int i = 0; i < finalResult.length(); i++) {

				String joname = null;
				String jophone = null;
				try {
					
					joname = ((JSONObject) finalResult.get(i))
							.getString("name");
					jophone = ((JSONObject) finalResult.get(i))
							.getString("phoneNumber");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Toast.makeText(GetInfo.this, e.toString(),
							Toast.LENGTH_SHORT).show();	
				}
				if (joname != null && jophone != null && !db.friendExists(jophone)){
					db.insertFriendTable(joname, jophone, -1);
				}

			}
			db.close();
			
			Intent i = new Intent(GetInfo.this,MainActivity.class);
			startActivity(i);
			finish();
			return status;
		}
		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			Toast.makeText(getApplicationContext(), values[0], Toast.LENGTH_SHORT).show();
		}

		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			
			
			super.onPostExecute(result);
			
		}
	}

}
