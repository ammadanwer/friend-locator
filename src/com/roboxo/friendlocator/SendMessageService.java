package com.roboxo.friendlocator;

import com.roboxo.friendlocator.Database.FriendLocatorDB;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.telephony.SmsManager;

public class SendMessageService extends Service {
	Location c;
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		String destination = intent.getStringExtra("destination");
		//String msg = intent.getStringExtra("is");
		LocationManager locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		c = locationManager
				.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		new SendSMS().execute(destination);
		
		return super.onStartCommand(intent, flags, startId);
	}

	private class SendSMS extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub
			FriendLocatorDB db = new FriendLocatorDB(getApplicationContext());
			boolean allow = false;
			db.open();
			allow = db.isFriendAllowed(params[0]);		//change this before submission
			db.close();
			SmsManager smsManager = SmsManager.getDefault();
			if(allow){		// if allow hen send him my location
			smsManager.sendTextMessage(
					params[0],					
					null,
					"FriendLocator##LocationResponseplz###"
							+ c.getLatitude() + "#" + c.getLongitude(),
					null, null);
			
			}
			else		// else dont send location
			{
				smsManager.sendTextMessage(
						params[0],
						null,
						"FriendLocator##LocationResponseplz###"+"Sorry not allowed",
						null, null);
			}
			return null;
		}
		

	}
}
