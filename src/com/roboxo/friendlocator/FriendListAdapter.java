package com.roboxo.friendlocator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.roboxo.friendlocator.Database.FriendLocatorDB;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class FriendListAdapter extends ArrayAdapter<User> {

	Context mContext;
	ArrayList<User> users;
	FriendLocatorDB db;

	public FriendListAdapter(Context context, int textViewResourceId,
			List<User> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		mContext = context;
		users = (ArrayList<User>) objects;
		db = new FriendLocatorDB(mContext); 

	}

	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row;
		if (convertView == null) {
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(R.layout.list_item, parent, false);
		} else {
			row = (View) convertView;
		}
		TextView name = (TextView) row.findViewById(R.id.tvName);
		name.setText(users.get(position).name);
		TextView tb = (TextView) row.findViewById(R.id.tvAllow);
		Button b = (Button) row.findViewById(R.id.bSendInvite);
		if (users.get(position).allowed == -1) {
			tb.setVisibility(View.GONE);
			b.setVisibility(View.VISIBLE);
		}
			else if (users.get(position).allowed == 0) {
				b.setVisibility(View.GONE);
				tb.setVisibility(View.VISIBLE);
				tb.setText("Request sent");
				
		} else {
			tb.setVisibility(View.VISIBLE);
			b.setVisibility(View.GONE);

		}
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				new sendRequest().execute(users.get(position).phone);
				
				db.open();
				db.changeAllowStatusToSent(users.get(position).phone);
				db.close();
				convertView.refreshDrawableState();
				
			}
		});
		return row;
	}

	public class sendRequest extends AsyncTask<String, Void, Void> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			Toast.makeText(mContext, "senrq", Toast.LENGTH_LONG).show();
		}

		@Override
		protected Void doInBackground(String... arg0) {
			// TODO Auto-generated method stub

			SharedPreferences p = PreferenceManager
					.getDefaultSharedPreferences(mContext);
			final String simID = p.getString("phone", "");
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://friendlocator.web44.net/sendRequest.php");

			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs
					.add(new BasicNameValuePair("myPhone", simID.toString()));
			nameValuePairs.add(new BasicNameValuePair("friendPhone", arg0[0]));

			try {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent()));
				String abc = (reader.readLine());
				//Toast.makeText(mContext, abc.toString(), Toast.LENGTH_LONG)
						//.show();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

	}

}
