package com.roboxo.friendlocator.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FriendLocatorDB {
	private static final String DATABASE_NAME = "FLDatabase";
	
	private static final String DATABASE_FRIEND_TABLE = "FriendTable";
	public static final String NAME = "name";
	public static final String PHONE = "phone";
	public static final String ALLOWED = "allowed";			// -1 means not allowed, 0 means request sent, 1 means allowed
	private static final int DATABASE_VERSION = 2;
	
	private DbHelper OurHelper;
	private final Context ourContext;
	private SQLiteDatabase ourDatabase;
	
	private static class DbHelper extends SQLiteOpenHelper 
	{

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub

		}

		@Override
		public void onCreate(SQLiteDatabase arg0) {
			// TODO Auto-generated method stub
			arg0.execSQL("CREATE TABLE " + DATABASE_FRIEND_TABLE + " (" + NAME
					+ " TEXT, " + PHONE
					+ " TEXT, " + ALLOWED + " INTEGER);"

			);

		}

		@Override
		public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
			// TODO Auto-generated method stub
			arg0.execSQL("DROP TABLE IF EXISTS " + DATABASE_FRIEND_TABLE);
			onCreate(arg0);

		}
	}
	public FriendLocatorDB(Context c) {
		ourContext = c;
	}

	public FriendLocatorDB open() {
		OurHelper = new DbHelper(ourContext);
		ourDatabase = OurHelper.getWritableDatabase();
		return this;

	}

	public void close() {
		OurHelper.close();
	}
	public long insertFriendTable(String myName, String phone,int allowed) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(NAME, myName);
		cv.put(PHONE, phone);
		cv.put(ALLOWED, allowed);

		return ourDatabase.insert(DATABASE_FRIEND_TABLE, null, cv);

	}
	public Cursor getFriendTable() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { NAME,PHONE,ALLOWED };
		return ourDatabase.query(DATABASE_FRIEND_TABLE, columns, null, null,null,null,null);
	}
	public int getFriendTableCount() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { NAME,PHONE,ALLOWED };
		Cursor c = ourDatabase.query(DATABASE_FRIEND_TABLE, columns, null, null,null,null,null);
		c.moveToFirst();
		return c.getCount();
	}
	public String getCorrespondingFriendName(String phones) {
		// TODO Auto-generated method stub
		String[] columns = new String[] { NAME,PHONE,ALLOWED };
		Cursor c = ourDatabase.query(DATABASE_FRIEND_TABLE, columns, PHONE +"='"+ phones+"'",null,null,null,null);
		c.moveToFirst();
		return c.getString(0);
	}

	public void allowFriend(String number) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(ALLOWED, "1");
		ourDatabase.update(DATABASE_FRIEND_TABLE, cv, PHONE + " = '" + number+"'", null);
		
	}
	public void disallowFriend(String number) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(ALLOWED, "-1");
		ourDatabase.update(DATABASE_FRIEND_TABLE, cv, PHONE + " = '" + number+"'", null);
		
	}
	public void changeAllowStatusToSent(String number)
	{
		ContentValues cv = new ContentValues();
		cv.put(ALLOWED, "0");
		ourDatabase.update(DATABASE_FRIEND_TABLE, cv, PHONE + " = '" + number+"'", null);
	}

	public boolean isFriendAllowed(String phones) {
		// TODO Auto-generated method stub
		String[] columns = new String[] { NAME,PHONE,ALLOWED };
		Cursor c = ourDatabase.query(DATABASE_FRIEND_TABLE, columns, PHONE +"='"+ phones+"'",null,null,null,null);
		c.moveToFirst();
		if(c.getInt(2)==1)
			return true;
		else
			return false;
	}

	public Cursor getFriendswithSentRequest() {
		// TODO Auto-generated method stub
		String[] columns = new String[] { NAME,PHONE,ALLOWED };
		Cursor c = ourDatabase.query(DATABASE_FRIEND_TABLE, columns, ALLOWED + "=" + 0 ,null,null,null,null);
		c.moveToFirst();
		
		return c;
	}

	public boolean friendExists(String phone) {
		// TODO Auto-generated method stub
		String[] columns = new String[] { NAME,PHONE,ALLOWED };
		Cursor c = ourDatabase.query(DATABASE_FRIEND_TABLE, columns, PHONE + "='" + phone + "'",null,null,null,null);
		c.moveToFirst();
		if(c.getCount()==0)
			return false;
		else
			return true;
	}
}